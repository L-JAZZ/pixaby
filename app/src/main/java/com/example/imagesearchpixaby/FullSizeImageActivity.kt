package com.example.imagesearchpixaby

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_full_size_image.*

class FullSizeImageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_full_size_image)

            val intent = intent.getStringExtra("image_url")

            Picasso.get().load(intent).into(imageView_full_size)
    }
}
