package com.example.imagesearchpixaby

import com.example.imagesearchpixaby.ApiServices.ImageApiServiceLoader
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.imagesearchpixaby.adapter.ImageViewAdapter
import com.example.imagesearchpixaby.apiapp.Image
import com.example.imagesearchpixaby.apiapp.ImageResponse
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadImage()
        /*Facade*/
    }

    private fun loadImage(){
        recylerView_images.layoutManager = LinearLayoutManager(this)
        ImageApiServiceLoader(onSuccess = {
            response ->
                showImages(response)
                dataSearch(response)
        }, onErrorAction ={
            Log.d("MainActivity", it.message)
        }).loadImages()
    }

    private fun showImages(response: ImageResponse){
        recylerView_images.adapter = response.hits.let {
            //to show full size
            ImageViewAdapter(hits = it, onItemClick = {
                val intent = Intent(this, FullSizeImageActivity::class.java)
                intent.putExtra("IMAGE_URL", it.largeImageURL)
                startActivity(intent)
            })
        }
    }

    private fun dataSearch(response: ImageResponse){
        val search = findViewById<SearchView>(R.id.txt_search)
        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(inputedText: String): Boolean {
                if (inputedText.isEmpty()){
                    showImages(response)
                }
                else{
                    search(response,inputedText)
                }
                return false
            }
        })
    }


    private fun search(response: ImageResponse, tag: String){
        var byTag = tag
        val list = ArrayList<Image>()

        for (hit in response.hits){
            if (hit.tags.contains(byTag,false)){
                list.add(hit)
            }
        }
        recylerView_images.adapter = list.let {
            ImageViewAdapter(hits = it, onItemClick = {
                val intent = Intent(this, FullSizeImageActivity::class.java)
                intent.putExtra("IMAGE_URL", it.largeImageURL)
                startActivity(intent)
            })
        }
    }
}
