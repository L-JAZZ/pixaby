/*
package com.example.imagesearchpixaby.data

import android.content.Intent
import android.util.Log
import android.widget.SearchView
import com.example.imagesearchpixaby.ApiServices.ImageApiServiceLoader
import com.example.imagesearchpixaby.FullSizeImageActivity
import com.example.imagesearchpixaby.R
import com.example.imagesearchpixaby.adapter.ImageViewAdapter
import com.example.imagesearchpixaby.apiapp.Image
import com.example.imagesearchpixaby.apiapp.ImageResponse
import kotlinx.android.synthetic.main.activity_main.*

class Facade {
    fun loadImage(){
        ImageApiServiceLoader(onSuccess = {
                response ->
            displayImages(response)
            dataSearch(response)
        }, onErrorAction ={
            Log.d("MainActivity", it.message)
        }).loadImages()
    }

    private fun dataSearch(response: ImageResponse){
        val search = findViewById<SearchView>(R.id.txt_search)
        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(inputedText: String): Boolean {
                if (inputedText.isEmpty()){
                    displayImages(response)
                }
                else{
                    search(response,inputedText)
                }
                return false
            }
        })
    }

    private fun displayImages(response: ImageResponse){
        recylerView_images.adapter = response.hits.let {
            //to show full size
            ImageViewAdapter(hits = it, onItemClick = {
                val intent = Intent(this, FullSizeImageActivity::class.java)
                intent.putExtra("IMAGE_URL", it.largeImageURL)
                startActivity(intent)
            })
        }
    }
    private fun search(response: ImageResponse, tag: String){
        var byTag = tag
        val list = ArrayList<Image>()

        for (hit in response.hits){
            if (hit.tags.contains(byTag,false)){
                list.add(hit)
            }
        }
        recylerView_images.adapter = list.let {
            ImageViewAdapter(hits = it, onItemClick = {
                val intent = Intent(this, FullSizeImageActivity::class.java)
                intent.putExtra("IMAGE_URL", it.largeImageURL)
                startActivity(intent)
            })
        }
    }
}*/
