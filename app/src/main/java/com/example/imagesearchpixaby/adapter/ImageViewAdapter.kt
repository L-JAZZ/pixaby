package com.example.imagesearchpixaby.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.imagesearchpixaby.R
import com.example.imagesearchpixaby.apiapp.Image

import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.image_box.view.*


class ImageViewAdapter (
    private val hits: List<Image> = listOf(),
    var onItemClick: (Image) ->Unit):

    RecyclerView.Adapter<ImageViewAdapter.ItemViewHolder>(){

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.image_box, parent, false)
            return  ItemViewHolder(view)
        }

        override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
            holder.bindItem(hits[position])
        }

        override fun getItemCount(): Int {
            return hits.count()
        }

        inner class ItemViewHolder(private val view: View)
            :RecyclerView.ViewHolder(view){
            fun bindItem(item: Image){
                Picasso.get().load(item.largeImageURL).into(view.imageView)
                view.txt_tags.text = "Tag: " + item.tags
                view.txt_likes.text = "Likes:" + item.likes
                view.setOnClickListener {
                    onItemClick(item)
                }
            }
        }
    }