package com.example.imagesearchpixaby.apiapp

class ImageLoader(
    private val onSuccess: (ImageResponse) -> Unit,
    private val onError: (Throwable) -> Unit,
    private val key: String
)