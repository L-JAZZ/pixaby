package com.example.imagesearchpixaby.apiapp

data class Image(
    val likes: Int,
    val tags:String,
    val previewURL:String,
    val largeImageURL: String,
    val id:Int
)
data class ImageResponse(
    val hits: List<Image>
)


