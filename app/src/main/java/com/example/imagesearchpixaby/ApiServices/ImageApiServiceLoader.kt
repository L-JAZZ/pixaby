package com.example.imagesearchpixaby.ApiServices

import com.example.imagesearchpixaby.apiapp.ApiFactory
import com.example.imagesearchpixaby.apiapp.ImageResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ImageApiServiceLoader(
    private val onSuccess:(ImageResponse)->Unit,
    private val onErrorAction: (Throwable)->Unit
){
    fun loadImages(){
        ApiFactory.getApiService()
            .getAllDataByKey(API_KEY)
            .enqueue(object : Callback<ImageResponse> {
                override fun onResponse(
                    call: Call<ImageResponse>,
                    response: Response<ImageResponse>
                ) {
                    onSuccess(response.body()!!)
                }

                override fun onFailure(call: Call<ImageResponse>, t: Throwable) {
                    onErrorAction(t)
                }
            })
    }
}

const val API_KEY="3588873-8bb0e70fdfcef7f31eee25461"
//https://pixabay.com/api/?key=3588873-8bb0e70fdfcef7f31eee25461



